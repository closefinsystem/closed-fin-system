import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '../views/Login.vue'
import WalletView from '../views/Wallet.vue'
import RegisterView from '../views/Register.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'Login',
      component: LoginView
    },
    {
      path: '/wallet',
      name: 'Wallet',
      component: WalletView
    },
    {
      path: '/register',
      name: 'Register',
      component: RegisterView
    }
  ]
})

export default router